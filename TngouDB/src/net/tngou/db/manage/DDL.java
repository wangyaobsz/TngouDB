package net.tngou.db.manage;

import net.tngou.db.lucene.LuceneManage;
import net.tngou.db.util.I18N;
import net.tngou.db.util.ResultSet;


/**
 * 数据定义语言（DDL）

       用于创建、修改、和删除数据库内的数据结构，
       如：
       1：创建和删除数据库(CREATE  DATABASE || DROP  DATABASE)；
       2：创建、修改、重命名、删除表(CREATE  TABLE || ALTER  TABLE|| RENAME TABLE||DROP  TABLE)；
       3：创建和删除索引(CREATEINDEX  || DROP INDEX)

 * @author tngou
 *
 */
public class DDL extends SQL {

	
	

	/**
	 * 
	* @Title: create
	* @Description: 创建数据表
	* @param @param tableName    表名称
	* @return void    返回类型
	* @throws
	* 
	* CREATE TABLE  tb_name
	* 
	 */
	public ResultSet create() {
		String table = request.getParameterString("table");
		if(!luceneManage.createTable( table))
		{
//			response.setStatus(status);
			response.setMsg(I18N.getValue("create_false",table));
		}
		else
		{
			response.setMsg(I18N.getValue("create_true",table));
		}
	    
		return response;
	}
	
	
	/**
	 * 
	* @Title: drop
	* @Description: 删除数据表
	* @param @param table   表名称
	 * @return 
	* @return void    返回类型
	* @throws
	* 
	* DROP   TABLE  tb_name
	* 
	 */
	public ResultSet drop() {
		String table = request.getParameterString("table");
		if(!luceneManage.dropTable(table))
		{
			response.setMsg(I18N.getValue("drop_false",table));
		}
		else
		{
			response.setMsg(I18N.getValue("drop_true",table));
		}
		return response;
	}
	
	
	/**
	 * 
	* @Title: rename
	* @Description: 重命名数据表
	* @param @param old_table    旧数据表
	* @param @param new_table    新数据表
	 * @return 
	* @return void    返回类型
	* @throws
	* 
	* RENAME TABLE  old_tb_name TO new_tb_name
	* 
	 */
	public ResultSet rename() {
		String old_table = request.getParameterString("old_table");
		String new_table = request.getParameterString("new_table");
		if(!luceneManage.renameTable(old_table, new_table))
		{
			response.setMsg(I18N.getValue("rename_false",old_table,new_table,old_table,new_table));
		}
		else
		{
			response.setMsg(I18N.getValue("rename_true",old_table,new_table));
		}
		return response;
	}
}
